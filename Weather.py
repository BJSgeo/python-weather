# An application for getting current weather at different locations
# Copyright (c) 2019 Ben Goldberg
# Import libraries
import urllib.request
import json
import datetime
import tkinter as tk
from PIL import Image, ImageTk
from tkinter.colorchooser import askcolor
import time
from threading import Thread
import pytz # use pip install
from timezonefinder import TimezoneFinder # You can download this using pip install
# Initialize settings
locations = {}
activebg = ""
bg = ""
current = []
# Function to load settings from file:
def loadSettings():
    global locations,activebg,bg,current
    with open("settings.config", "r") as f:
        lines = f.readlines()
        locationFound = False
        for lineNum in range(len(lines)):
            print(lineNum)
            lines[lineNum] = lines[lineNum].replace("\n","")
            if lines[lineNum].startswith("# "):
                # that's a comment in the file, do nothing
                pass
            elif lines[lineNum].startswith("bg:"):
                bg = lines[lineNum].split(":")[1]
            elif lines[lineNum].startswith("activebg:"):
                activebg = lines[lineNum].split(":")[1]
            else:
                splot = lines[lineNum].split(":") # splot: past tense of split (JK :D)
                coords = splot[1].split(",")
                locations[splot[0]] = [float(coords[0]),float(coords[1])]
                if (not locationFound):
                    current = [float(coords[0]),float(coords[1])]
                    locationFound = True
# Call the function
loadSettings()
# Class for a button that changes color when hovered
class HoverButton(tk.Button):
    # This inherits from the tk.Button class
    def __init__(self, master=None, cnf={}, **kw):
        # Initialize a Button
        tk.Button.__init__(self,master=master,**kw)
        # On hover, call enter()
        self.bind("<Enter>",self.enter)
        # On stop hovering, call leave()
        self.bind("<Leave>",self.leave)
    def enter(self,e):
        # call _enter() in a seperate Thread
        self.enterT = Thread(target=self._enter)
        self.enterT.start()
    def leave(self,e):
        # tell _enter to stop:
        self.left = True
        # start _leave
        self.leaveT = Thread(target=self._leave)
        self.leaveT.start()
    def _enter(self):
        # Go from the background color to the active background color slowly
        self.left = False
        first = (int(bg[1]+bg[2],16),int(bg[3]+bg[4],16),int(bg[5]+bg[6],16))
        intermediate=first
        second=(int(activebg[1]+activebg[2],16),int(activebg[3]+activebg[4],16),int(activebg[5]+activebg[6],16))
        rs = (second[0]-first[0]) / 25
        gs = (second[1]-first[1]) / 25
        bs = (second[2]-first[2]) / 25
        self.configure(fg="#ffffff")
        for i in range(26):
            if self.left:
                return
            time.sleep(0.00001)
            r=int(first[0]+(rs*i))
            g=int(first[1]+(gs*i))
            b=int(first[2]+(bs*i))
            self.configure(bg="#"+hex(r).replace("0x","").zfill(2)+hex(g).replace("0x","").zfill(2)+hex(b).replace("0x","").zfill(2))
            self.update()
    def _leave(self):
        # Go from the active background color to the background color slowly
        second = (int(bg[1]+bg[2],16),int(bg[3]+bg[4],16),int(bg[5]+bg[6],16))
        first=(int(activebg[1]+activebg[2],16),int(activebg[3]+activebg[4],16),int(activebg[5]+activebg[6],16))
        intermediate=first
        rs = (second[0]-first[0]) / 25
        gs = (second[1]-first[1]) / 25
        bs = (second[2]-first[2]) / 25
        self.configure(fg="#000000")
        for i in range(26):
            time.sleep(0.00001)
            r=int(first[0]+(rs*i))
            g=int(first[1]+(gs*i))
            b=int(first[2]+(bs*i))
            self.configure(bg="#"+hex(r).replace("0x","").zfill(2)+hex(g).replace("0x","").zfill(2)+hex(b).replace("0x","").zfill(2))
            self.update()
# time zone finder
tf = TimezoneFinder()
# Make a Tkinter window
window = tk.Tk()
window.config(bg=bg)
# Do some stuff to make weather/settings fill window
window.grid_rowconfigure(0,weight=1)
window.grid_columnconfigure(0,weight=1)
# Make a frame (a container) for the weather
weather = tk.Frame(master=window)
# Make the background white
weather.config(bg=bg)
# Display it
weather.grid(row=0,column=0,sticky="nsew")
# Another frame for the settings
settings = tk.Frame(master=window)
# Make the background white
settings.config(bg=bg)
# Display it
settings.grid(row=0,column=0,sticky="nsew")
# But we want the weather on top to start:
weather.tkraise()
# screen size: 480x300
window.geometry("480x300")
# get icon for weather
weather_icon = ImageTk.PhotoImage(Image.open("sunny_color.png"))
# set icon for window to weather icon:
window.tk.call("wm", "iconphoto", window._w, weather_icon)
# And the window should be titled Weather
window.title("Weather")
## WEATHER STUFF FOLLOWS ##
# Index of current weather in data (0 is now)
index = 0
# Set current image variable originally to the refresh icon
img = ImageTk.PhotoImage(Image.open("refresh_color.png"))
# Dictionary of images to save references
images = {"refresh" : ImageTk.PhotoImage(Image.open("refresh_color.png")),
          "sunny" : ImageTk.PhotoImage(Image.open("sunny_color.png")),
          "partly-sunny" : ImageTk.PhotoImage(Image.open("partly-sunny_color.png")),
          "clear" : ImageTk.PhotoImage(Image.open("clear.png")),
          "snow" : ImageTk.PhotoImage(Image.open("snow_color.png")),
          "cloudy" : ImageTk.PhotoImage(Image.open("cloudy_color.png")),
          "thunderstorm" : ImageTk.PhotoImage(Image.open("thunderstorm_color.png")),
          "rainy" : ImageTk.PhotoImage(Image.open("rainy_color.png")),
          "download" : ImageTk.PhotoImage(Image.open("download_color.png")),
          "fog" : ImageTk.PhotoImage(Image.open("fog_color.png")),
          "frost" : ImageTk.PhotoImage(Image.open("frost_color.png"))}
# And night time images:
images_night = {"refresh" : ImageTk.PhotoImage(Image.open("refresh_color.png")),
          "sunny" : ImageTk.PhotoImage(Image.open("sunny_color.png")),
          "partly-sunny" : ImageTk.PhotoImage(Image.open("night-partly-cloudy.png")),
          "clear" : ImageTk.PhotoImage(Image.open("clear.png")),
          "snow" : ImageTk.PhotoImage(Image.open("night-snow.png")),
          "cloudy" : ImageTk.PhotoImage(Image.open("night-cloudy.png")),
          "thunderstorm" : ImageTk.PhotoImage(Image.open("night-thunderstorm.png")),
          "rainy" : ImageTk.PhotoImage(Image.open("night-rain.png")),
          "download" : ImageTk.PhotoImage(Image.open("download_color.png")),
          "fog" : ImageTk.PhotoImage(Image.open("fog_color.png")),
          "frost" : ImageTk.PhotoImage(Image.open("frost.png"))}
# Frame (container) for refresh and latitude and longitude
top_frame = tk.Frame(weather)
top_frame.configure(bg=bg)
# Button for refresh:
b_refresh = HoverButton(top_frame)
# Refresh image:
refresh = Image.open("refresh_color.png")
# Resized:
refresh.thumbnail((40,30))
# And converted so Tkinter can use it
refresh = ImageTk.PhotoImage(refresh)
# Tell the button to use the image
b_refresh.config(image=refresh, width=41, height=31)
# And make it flat instead of Tkinter's UGLY borders
b_refresh.config(relief='flat',highlightthickness=0,bd=0,bg=bg,
                 activebackground=activebg,activeforeground="#ffffff")
# And put it on the left side
b_refresh.pack(side="left")

# Latitude and longitude dropdown:
# Variable for location
location = tk.StringVar()
# Set it to the first item in the dictionary (a dictionary is like a list)
location.set(list(locations.keys())[0])
# Function to set the current weather location
def set_location(*args):
    global current
    current = locations[location.get()]
    loadWeather("use_cached", current[0], current[1])
    printHourly("current")
# Make it so that when it's changed, it tells the program to change the location
location.trace("w", set_location)
# Down arrow for the dropdown
down = Image.open("down.png")
# Resize:
down.thumbnail((10,16))
# Turn it into a Tkinter-compatible image
down = ImageTk.PhotoImage(down)
# Create a dropdown
dropdown = tk.OptionMenu(top_frame, location, *list(locations.keys()))
# With a white background (grey when hovered), custom font (my favorite font),
# no indicator (but a down arrow instead), and flat.
dropdown.configure(bg=bg,activebackground=activebg,font=("Comfortaa",10),
                   indicatoron=0,compound='right',image=down,relief='flat',
                   highlightthickness=0,bd=0,activeforeground="#ffffff")
dropdown["menu"].configure(bg=bg,font=("Comfortaa",10),relief='flat',
                           bd=0,activebackground=activebg,activeforeground="#ffffff")
# Function to switch to settings
def switchToSettings():
    settings.tkraise()
    # set title of window to "settings"
    window.title("Weather - Settings")
# Icon for settings button
settingsIcon = Image.open("settings.png")
# Resize
settingsIcon.thumbnail((36,30))
# Make Tkinter-compatible
settingsIcon = ImageTk.PhotoImage(settingsIcon)
# Settings button
settingsButton = HoverButton(top_frame,relief='flat',bg=bg,image=settingsIcon,
                   highlightthickness=0,bd=0,activebackground=activebg,
                   command=switchToSettings,activeforeground="#ffffff")
# Put the settings button on the right
settingsButton.pack(side="right")
# Put the dropdown on the right
dropdown.pack(side="right")
# And display the frame
top_frame.pack(pady=5, fill="x")

# Set up a label (basically a text & image container) to display the image
l = tk.Label(weather, image=img)
# Make the background white
l.configure(bg=bg)
# And pack it (display it) with padding on the Y axis of 5px (this is why the image is not at the top)
l.pack(pady=5)

# Set up a label (basically a text & image container) to display text (I am using
# tk.Message here instead of tk.Label because tk.Message wraps text, but same concept)
l_text = tk.Message(weather,text="Now",font=("Comfortaa", 10, ""),width=480,bg=bg)
# And pack it (display it) with padding on the Y axis of 10px (this is why the text has padding)
l_text.pack()

# Display the temperature!
# Create a frame (a container) for the temperature
temp_frame = tk.Frame(weather,bg=bg)
# Add a temperature label (we talked about labels earlier)
temp = tk.Label(temp_frame, text="--", bg=bg)
# And display it on the left side
temp.pack(side="left")
# And make the font my favorite font (Comfortaa, from Google Fonts ;D)
temp.configure(font=("Comfortaa", 19, "bold"))
# Load the image for degrees Fahrenheit
F_img = Image.open("fahrenheit.png")
# Make it 21x18
F_img.thumbnail((21, 18))
# Get a Tkinter-compatible image
F_img = ImageTk.PhotoImage(F_img)
# Make another label for the image and set the label's image to F_img
degF = tk.Label(temp_frame, image=F_img, bg=bg)
# Display it on the right
degF.pack(side="right")
# And display the frame using pack()
temp_frame.pack()

# Frame (container) for buttons for selecting hours
hourButtons = tk.Frame(master=weather, width=480, bg=bg)

# Button for left arrow:
b_left = HoverButton(hourButtons)
# Left arrow image:
left = Image.open("left.png")
# Resized:
left.thumbnail((25,22))
# And converted so Tkinter can use it
left = ImageTk.PhotoImage(left)
# Tell the button to use the image
b_left.config(image=left, width=25, height=22)
# activebg color when clicked, white when not clicked, flat button
b_left.config(relief='flat',highlightthickness=0,bd=0,bg=bg,
              activebackground=activebg,activeforeground="#ffffff")
# And put it on the first row & column (using grid() instead of pack()) with
# X axis padding
b_left.grid(column=0, row=0, padx=50)

# Button for current weather:
b_current = HoverButton(hourButtons, text="Current Weather",
                      font=("Comfortaa", 12, ""))
# activebg color when clicked, white when not clicked, flat button
b_current.config(relief='flat',highlightthickness=0,bd=0,bg=bg,
                 activebackground=activebg,activeforeground="#ffffff")
# Put it on the second column & first row
b_current.grid(column=1, row=0)

# Button for right arrow:
b_right = HoverButton(hourButtons)
# Right arrow image:
right = Image.open("right.png")
# Resized:
right.thumbnail((25,22))
# And converted so Tkinter can use it
right = ImageTk.PhotoImage(right)
# Tell the button to use the image
b_right.config(image=right, width=25, height=22)
# activebg color when clicked, white when not clicked, flat button
b_right.config(relief='flat',highlightthickness=0,bd=0,bg=bg,
               activebackground=activebg,activeforeground="#ffffff")
# And put it on the third column and first row with X axis padding
b_right.grid(column=2,row=0,padx=50)

hourButtons.pack(side="bottom")
# current weather data: glorified nothing!
WEATHER = ""
# Timezone
tz = pytz.timezone("UTC")
# But when we call this function, it becomes something ;)
def loadWeather(mode, lat, lon):
    global WEATHER, l, img, tz # Use the global variables (across the program) instead of keeping them inside the function
    # If we didn't do this, img, weather, and l would only be accessible inside this function
    if mode == "download": # Download weather from online
        l.configure(image=images["refresh"]) # Set l's current image to the refresh symbol
        window.update() # And update the window so the new image appears
        # Open the url https://api.weather.gov/points/<LATITUDE>,<LONGITUDE> and call it f below
        # The benefit of this with statement is f is automatically closed at the end
        with urllib.request.urlopen("https://api.weather.gov/points/"+str(lat)+","+str(lon)) as f:
            # Get the JSON (UTF-8 encoded) from the site
            msgSecond = f.read().decode('utf-8')
        # And parse it:
        x = json.loads(msgSecond)
        # get timezone:
        tz = pytz.timezone(tf.timezone_at(lng=lon, lat=lat))
        # Now load *another* URL from the first one! The first URL gives us
        # the URL to the regular forecast and the hourly forecast, so visit that
        with urllib.request.urlopen(x['properties']['forecastHourly']) as f:
            # And finally get the weather!
            WEATHER = f.read().decode('utf-8')
            with open(str(lat)+str(lon)+".json", "w") as file: # Cache our current weather
                file.write(WEATHER)
    elif mode == "use_cached": # Use cached weather copy
        try: # Get cached copy
            with open(str(lat)+str(lon)+".json", "r") as file: # Get our cached weather
                WEATHER = file.read()
                tz = pytz.timezone(tf.timezone_at(lng=lon, lat=lat))
        except:
            # Oh no! Cached copy doesn't exist and threw an error. Let's resort to online.
            l_text.configure(text="Cached copy not available -- trying to load from the Internet...")
            loadWeather("download", lat, lon) # pass back location
    # Set img to the download image
    img = images["download"]
    # and set the label's image to img
    l.configure(image=img)


# Get the time (aka parse out all the numbers) -- thanks StackExchange!
def getTime(str):
    return [int(s) for s in str.replace(":","-").replace("T","-").split(sep='-') if s.isdigit()]


# This function actually displays the hourly weather.
def printHourly(dir):
    # Again global variables (see above)
    global img, WEATHER, index, temp, tz
    # Parse the weather that loadWeather() so nicely got for us
    y = json.loads(WEATHER)
    # if the argument passed is "plus", move an hour ahead
    if dir == "plus":
        index += 1
    # if the argument passed is "minus", move an hour behind
    elif dir == "minus":
        index -= 1
    # if the argument passed is "nothing", same hour
    elif dir == "nothing":
        pass
    # if the argument passed is "current", go to current time
    elif dir == "current":
        # get current time
        now = datetime.datetime.now(tz=tz)
        # set a variable to False to see if current weather is found
        found = False
        # go through all possible hours
        for i in range(156):
            # get the time for that hour
            time = getTime(y['properties']['periods'][i]['startTime'])
            year = time[0]
            month = time[1]
            day = time[2]
            hour = time[3]
            # if the time is the same as the current time:
            if (str(now.year) == str(year) and str(now.month) == str(month)
                and str(now.day) == str(day) and str(now.hour) == str(hour)):
                index = i # set index
                found = True
                break # break out of the loop
        # else do NOTHING! no else statement
        if not found:
            l_text.configure(text="Weather outdated! Press the reload button in the top left corner to reload.")
            return
    # Get the time
    time = getTime(y['properties']['periods'][index]['startTime'])
    year = time[0]
    month = time[1]
    day = time[2]
    hour = time[3]
    # And the forecast
    forecast = y['properties']['periods'][index]['shortForecast']
    # And the temperature
    temp_F = y['properties']['periods'][index]['temperature']
    temp.configure(text=str(temp_F))
    # Get the string of the time and forecast which will be printed and displayed
    string = str(month)+"-"+str(day)+"-"+str(year)+" at "+str(hour)+":00, the weather is "+forecast
    # Get time for index 0 (first weather segment aka when the weather data was downloaded)
    time = getTime(y['properties']['periods'][0]['startTime'])
    year = time[0]
    month = time[1]
    day = time[2]
    hour = time[3]
    # And say that's when we got the weather
    string += ". As of "+str(month)+"-"+str(day)+"-"+str(year)+" at "+str(hour)+":00"
    print(string)
    # Now set img (a variable) to the relevant image for the relevant time (day/night)
    if y['properties']['periods'][index]['isDaytime'] == True:
        if "Mostly Cloudy" in forecast:
            img = images['partly-sunny']
        elif "Partly Cloudy" in forecast:
            img = images['partly-sunny']
        elif "Mostly Sunny" in forecast:
            img = images['partly-sunny']
        elif "Partly Sunny" in forecast:
            img = images['partly-sunny']
        elif "Partly Cloudy" in forecast:
            img = images['partly-sunny']
        elif "Clear" in forecast:
            img = images['clear']
        elif "Sunny" in forecast:
            img = images['sunny']
        elif "Cloudy" in forecast:
            img = images['cloudy']
        elif "Snow" in forecast: # I really hope this happens
            img = images['snow'] # YAY!
        elif "thunder" in forecast.lower():
            img = images['thunderstorm']
        elif "windy" in forecast.lower():
            img = images['windy']
        elif "rain" in forecast.lower():
            img = images['rainy']
        elif "fog" in forecast.lower():
            img = images['fog']
        elif "frost" in forecast.lower():
            img = images['frost']
    elif y['properties']['periods'][index]['isDaytime'] == False:
        if "Mostly Cloudy" in forecast:
            img = images_night['partly-sunny']
        elif "Partly Cloudy" in forecast:
            img = images_night['partly-sunny']
        elif "Mostly Clear" in forecast:
            img = images_night['partly-sunny']
        elif "Partly Clear" in forecast:
            img = images_night['partly-sunny']
        elif "Partly Cloudy" in forecast:
            img = images_night['partly-sunny']
        elif "Clear" in forecast:
            img = images_night['clear']
        elif "Sunny" in forecast:
            img = images_night['sunny']
        elif "Cloudy" in forecast:
            img = images_night['cloudy']
        elif "Snow" in forecast: # I really hope this happens
            img = images_night['snow'] # YAY!
        elif "thunder" in forecast.lower():
            img = images_night['thunderstorm']
        elif "windy" in forecast.lower():
            img = images_night['windy']
        elif "rain" in forecast.lower():
            img = images_night['rainy']
        elif "fog" in forecast.lower():
            img = images_night['fog']
        elif "frost" in forecast.lower():
            img = images_night['frost']
    # Actually set the image:
    l.configure(image=img)
    # And the text:
    l_text.configure(text=string)
# Load the weather
loadWeather("use_cached", current[0], current[1])
# And display the current weather
printHourly("current")
# If right button pressed, get weather with +1 hour
b_right.config(command=lambda: printHourly("plus"))
# If left button pressed, get weather with -1 hour
b_left.config(command=lambda: printHourly("minus"))
# If current weather button pressed, get current weather
b_current.config(command=lambda: printHourly("current"))
# If refresh button pressed, load weather
b_refresh.config(command=lambda: loadWeather("download", current[0], current[1]))

## SETTINGS STUFF FOLLOWS ##

# Function to switch back to weather
def switchToWeather():
    weather.tkraise()
    # set title of window to "weather"
    window.title("Weather")

# Icon for weather button
weatherIcon = Image.open("sunny_color.png")
# Resize
weatherIcon.thumbnail((36,30))
# Make Tkinter-compatible
weatherIcon = ImageTk.PhotoImage(weatherIcon)
# Weather button
weather_b = HoverButton(settings,image=weatherIcon,relief='flat',bg=bg,
                   highlightthickness=0,bd=0,activebackground=activebg,
                   command=switchToWeather,activeforeground="#ffffff")
# Put the weather button on the top right
weather_b.pack(side="top",anchor="e")

# Label to say "SETTINGS CHANGE WHEN YOU CLOSE/OPEN WEATHER"
settingsLabel = tk.Label(settings,relief='flat',bg=bg,
                  text="SETTINGS UPDATE WHEN YOU CLOSE/OPEN WEATHER",
                  highlightthickness=0,bd=0,activebackground=activebg,
                  font=("Comfortaa",10),activeforeground="#ffffff")
settingsLabel.pack()

# Function to select background color
def setBgColor():
    # Find the line starting with bg: and replace the color
    with open("settings.config") as file:
        line = -1
        lines = file.readlines()
        for l in range(len(lines)):
            if lines[l].startswith("bg:"):
                line = l
                lines[l] = lines[l].replace("bg:","").replace("\n","")
        if line != -1:
            print(lines[line])
        color = askcolor()[1]
        lines[line] = "bg:"+color+"\n"
    with open("settings.config","w") as file:
        file.writelines(lines)

# Function to select clicked/hovered background color
def setActiveBgColor():
    # Find the line starting with activebg: and replace the color
    with open("settings.config") as file:
        line = -1
        lines = file.readlines()
        for l in range(len(lines)):
            if lines[l].startswith("activebg:"):
                line = l
                lines[l] = lines[l].replace("activebg:","").replace("\n","")
        if line != -1:
            print(lines[line])
        color = askcolor()[1]
        lines[line] = "activebg:"+color+"\n"
    with open("settings.config","w") as file:
        file.writelines(lines)

# Function to change location
def setLoc():
    # Find the line starting with <locationName>: and replace its coordinates
    location = selected.get()
    with open("settings.config") as file:
        line = -1
        lines = file.readlines()
        for l in range(len(lines)):
            if lines[l].startswith(location+":"):
                line = l
                lines[l] = lines[l].replace(location+":","").replace("\n","")
        if line != -1:
            print(lines[line])
        newLoc = locationInput.get()
        # Verify thatt coords are valid:
        try:
           for i in newLoc.split(","):
               a = float(i)
        except:
            locationInput.delete(0,"end")
            locationInput.insert(0,"Invalid Input!")
            return
        lines[line] = location+":"+newLoc+"\n"
        with open("settings.config","w") as file:
            file.writelines(lines)
        
# Function to show location
def showLocation(location):
    # Get the coordinates of the line starting with <location>
    with open("settings.config") as file:
        line = -1
        lines = file.readlines()
        for l in range(len(lines)):
            if lines[l].startswith(location+":"):
                line = l
                lines[l] = lines[l].replace(location+":","").replace("\n","")
        if line != -1:
            print(lines[line])
        locationInput.delete(0,"end")
        locationInput.insert(0,lines[line])

# Label to say "COLORS"
colors = tk.Label(settings,relief='flat',bg=bg,text="COLORS",
                  highlightthickness=0,bd=0,activebackground=activebg,
                  font=("Comfortaa",14),activeforeground="#ffffff")
colors.pack()

# Button to select background color:
selectBg = HoverButton(settings,text="Set Background Color",relief='flat',bg=bg,
                     highlightthickness=0,bd=0,activebackground=activebg,
                     command=setBgColor,activeforeground="#ffffff")
selectBg.pack()

# Button to select hovered over/clicked/active background color
selectActiveBg = HoverButton(settings,text="Set Hovered/Clicked Color",relief='flat',
                           bg=bg,highlightthickness=0,bd=0,activebackground=activebg,
                           command=setActiveBgColor,activeforeground="#ffffff")
selectActiveBg.pack()

# Label to say "LOCATIONS"
locationLabel = tk.Label(settings,relief='flat',bg=bg,text="LOCATIONS",
                  highlightthickness=0,bd=0,activebackground=activebg,
                  font=("Comfortaa",14),activeforeground="#ffffff")
locationLabel.pack()

# Label to say "EXISTING"
existingLabel = tk.Label(settings,relief='flat',bg=bg,text="EXISTING",
                  highlightthickness=0,bd=0,activebackground=activebg,
                  font=("Comfortaa",12),activeforeground="#ffffff")
existingLabel.pack()

# Frame for dropdown and input
locationFrame = tk.Frame(settings,bg=bg)

# Set location dropdown:
# Variable for selected location
selected = tk.StringVar()
# Set it to the first item in the dictionary (a dictionary is like a list)
selected.set(list(locations.keys())[0])
# Down arrow for the dropdown - we already made it above so we don't need
# to load it again!
# Create a dropdown
locationDropdown = tk.OptionMenu(locationFrame, location, *list(locations.keys()),
                                 command=showLocation)
# With a white background (grey when hovered), custom font (my favorite font),
# no indicator (but a down arrow instead), and flat.
locationDropdown.configure(bg=bg,activebackground=activebg,font=("Comfortaa",10),
                           indicatoron=0,compound='right',image=down,relief='flat',
                           highlightthickness=0,bd=0,activeforeground="#ffffff")
locationDropdown["menu"].configure(bg=bg,font=("Comfortaa",10),relief='flat',bd=0,
                                activebackground=activebg,activeforeground="#ffffff")
locationDropdown.grid(row=0,column=0)

# Input box for location
locationInput = tk.Entry(locationFrame,relief='solid',bg=bg,
                         highlightbackground=activebg,highlightcolor=activebg)
# Show "Coordinates"
locationInput.insert(0,"Insert Coordinates")
locationInput.grid(row=0,column=1)

# Button to change location coordinates
changeLoc = HoverButton(locationFrame,text="Update Location Coordinates",relief='flat',
                      bg=bg,highlightthickness=0,bd=0,activebackground=activebg,
                      command=setLoc,activeforeground="#ffffff")
changeLoc.grid(row=0,column=2)

locationFrame.pack()

# Label to say "NEW"
newLabel = tk.Label(settings,relief='flat',bg=bg,text="NEW",
                  highlightthickness=0,bd=0,activebackground=activebg,
                  font=("Comfortaa",12),activeforeground="#ffffff")
newLabel.pack()

# Frame for adding new location
newLocationFrame = tk.Frame(settings,bg=bg)

# Input for new location name
locName = tk.Entry(newLocationFrame,relief='solid',bg=bg,
                   highlightbackground=activebg,highlightcolor=activebg)
# Show "Location Name"
locName.insert(0,"Location Name")
# And display
locName.grid(row=0,column=0,padx=(0,5))

# Input for new location coordinates
locCoords = tk.Entry(newLocationFrame,relief='solid',bg=bg,
                   highlightbackground=activebg,highlightcolor=activebg)
# Show "Location GPS Coordinates"
locCoords.insert(0,"GPS Coordinates")
# And display
locCoords.grid(row=0,column=1,padx=(0,5))

# Function to add new location:
def addNewLocation():
    # Append new line to end of config
    with open("settings.config","a+") as file:
        newName = locName.get() # Get the value from the locName Entry
        newLoc = locCoords.get() # Get the value from the locCoords Entry
        # test to make sure the coords are in the format number,number
        try:
           for i in newLoc.split(","):
               a = float(i)
        except:
            locCoords.delete(0,"end")
            locCoords.insert(0,"Invalid Input!")
            return
        # and if they are, write it to the file
        file.write("\n"+newName+":"+newLoc)

# Button to add new location:
# Load icon
plus = Image.open("plus.png")
# Resize it
plus.thumbnail((25,25))
# And make it Tkinter-compatible
plus = ImageTk.PhotoImage(plus)
# Now make the button
plusButton = HoverButton(newLocationFrame,image=plus,relief='flat',highlightthickness=0,
               bd=0,bg=bg,activebackground=activebg,activeforeground="#ffffff",
               command=addNewLocation)
plusButton.grid(row=0,column=2)

newLocationFrame.pack()

# the end
window.mainloop()